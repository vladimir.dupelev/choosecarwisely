define("models/favTab", ["models/tab", "models/domain/selectedCarsList"], function (Tab, SelectedCarsList) {
  return Tab.extend({
    defaults: {
      label: "__Favs",
      route: "fav",
      content: "favTab"
    },

    initialize: function () {
      var self = this;

      var favList = new SelectedCarsList(null, {destroyOnDeselect: true, saveAllOnUpdate: true});
      self.set("favList", favList);
    },

    fetchContent: function () {
      var self = this;

      var favList = self.get("favList");
      favList.fetch();
    },

    totalCount: function () {
      return this.get("favList").length;
    }
  });
});