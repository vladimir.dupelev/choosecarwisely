define("models/carsTab", ["models/tab", "models/domain/selectedCarsList", "models/domain/stat"],
  function (Tab, SelectedCarsList, Stat) {
    return Tab.extend({
      defaults: {
        label: "__AllCars",
        route: "",
        content: "carsTab",
        filter: "all"
      },

      initialize: function () {
        var self = this;
        var carList = new SelectedCarsList(null, {saveOnSelect: true});
        self.set("carList", carList);

        var stat = new Stat();

        this.listenTo(carList, "change:selected", function (car) {
          if (car.get("selected")) {
            stat.incrementUsage(car.get("brand"));
          }
        }, this);
      },

      fetchContent: function () {
        this.get("carList").fetchAndMerge();
      },

      filterItem: function (item) {
        if (this.get("filter") === "all") {
          return true;
        }

        return item.get("brand").toLowerCase() === this.get("filter").toLowerCase();
      },

      count: function () {
        var self = this;

        if (this.get("filter") === "all") {
          return this.totalCount();
        }

        return this.get("carList").filter(function (item) {
          return item.get("brand").toLowerCase() === self.get("filter").toLowerCase();
        }).length;
      },

      totalCount: function () {
        return this.get("carList").length;
      }
    });
  });