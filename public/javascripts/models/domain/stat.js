define("models/domain/stat", function () {
  return Backbone.Model.extend({
    id: "stat",
    localStorage: new Backbone.LocalStorage("stat"),

    initialize: function (models, options) {
      this.fetch();
    },

    isNew: function () {
      return false;
    },

    incrementUsage: function (brand) {
      var self = this;
      self.clear({silent: true});
      self.fetch();
      var value = self.get(brand);
      value = value ? value + 1 : 1;
      self.set(brand, value);
      self.save();
    },

    getBrands: function () {
      return this.keys();
    },

    getCountByBrand: function () {
      return this.values();
    },

    clearStats: function () {
      this.clear();
      this.save();
    }
  });
});