define("models/domain/selectedCarsList", ["models/domain/car"], function (Car) {
  return Backbone.Collection.extend({
    model: Car,

    url: "/data/carList/",

    localStorage: new Backbone.LocalStorage("selectedCarsList"),

    initialize: function (models, options) {
      var self = this;
      options = options || {};

      this.on("change:selected", function (model) {
        self.processSelection(model, options);
      }, this);


      if (options.saveAllOnUpdate) {
        this.on("reset update", this.save, this);
      }
    },

    fetchAndMerge: function () {
      var self = this;

      self.fetch({ajaxSync: true, reset: true, silent: true}).done(function () {
        self.fetch({remove: false, silent: true});
        self.trigger("reset");
      });
    },

    processSelection: function (model, options) {
      if (model.get("selected")) {
        if (options.saveOnSelect) {
          this.save();
        }
      } else {
        if (options.destroyOnDeselect) {
          model.destroy();
        }
      }
    },

    save: function () {
      this.localStorage._clear();

      this.each(function (model) {
        if (model.get("selected")) {
          model.save();
        }
      });
    }
  });
});