define("models/appTabs", ["models/tab"], function (Tab) {
  return Backbone.Collection.extend({
    model: Tab,

    activateModel: function (activeModel) {
      this.each(function (model) {
        model.set("active", model === activeModel);
      }, this);

      activeModel.fetchContent();

      this.trigger("activation", activeModel);

      Backbone.history.navigate(activeModel.get("route"));
    },

    getActiveModel: function () {
      return this.findWhere({active: true});
    }

  });
});