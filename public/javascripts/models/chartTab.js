define("models/chartTab", ["models/tab", "models/domain/stat"],
  function (Tab, Stat) {
    return Tab.extend({
      defaults: {
        label: "__Charts",
        route: "chart",
        content: "chartTab"
      },

      initialize: function () {
        this.stat = new Stat();
      },

      fetchContent: function () {
        this.stat.fetch();
      },


      getBrands: function () {
        return this.stat.getBrands();
      },

      getCountByBrand: function () {
        return this.stat.getCountByBrand();
      },

      clearStats: function () {
        return this.stat.clearStats();
      }
    });
  });