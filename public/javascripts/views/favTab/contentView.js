define("views/favTab/contentView",
  ["views/view", "views/table/tableBody", "views/favTab/tableRow"],
  function (View, TableBody, TableRow) {
    return View.extend({
      model: null,

      initialize: function () {
        this.tableBody = null;

        this.listenTo(this.model.get("favList"), "update reset", this.updateCount);

        this.render();
      },

      destroyView: function () {
        if (this.tableBody) {
          this.tableBody.destroyView();
          this.tableBody = null;
        }

        View.prototype.destroyView.call(this);
      },

      render: function () {
        if (this.tableBody) {
          this.tableBody.destroyView();
          this.tableBody = null;
        }

        this.tableBody = new TableBody({
          model: this.model,
          collection: this.model.get("favList"),
          el: this.$("#favCarsTable").children("tbody").first(),
          RowViewCtor: TableRow
        });

        this.updateCount();

        return this;
      },

      updateCount: function () {
        $("#favCarsTotalCount").text(this.model.totalCount());
      }
    });
  });