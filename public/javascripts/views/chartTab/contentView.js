define("views/chartTab/contentView",
  ["views/view"],
  function (View) {
    return View.extend({
      model: null,

      initialize: function () {
        this.chartControl = null;

        this.render();
      },

      events: {
        "click #clearStats": "clearStats"
      },

      clearStats: function () {
        this.model.clearStats();
        this.render();
      },

      destroyView: function () {
        if (this.chartControl) {
          this.chartControl.destroy();
          this.chartControl = null;
        }

        View.prototype.destroyView.call(this);
      },

      render: function () {
        var self = this;

        if (self.chartControl) {
          self.chartControl.destroy();
          self.chartControl = null;
        }

        var ctx = self.$el.find("#selectionStatistics");
        self.chartControl = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: self.model.getBrands(),
            datasets: [
              {
                label: "Count",
                backgroundColor: "#A3D4FA",
                borderColor: "#91CBF9",
                borderWidth: 1,
                hoverBackgroundColor: "#54AEF5",
                hoverBorderColor: "#2196f3",
                data: self.model.getCountByBrand()
              }
            ]
          },
          options: {
            legend: false,
            scales: {
              yAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: "Count"
                },
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

        return this;
      }
    });
  });