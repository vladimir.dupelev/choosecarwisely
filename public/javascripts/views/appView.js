define("views/appView",
  ["views/view", "views/tabView", "views/tabContentView"],
  function (View, TabView, TabContentView) {
    return View.extend({
      collection: null,

      el: $("#app"),

      initialize: function () {
        this.$tabLabels = $("#tabs");
        this.$tabContent = $("#tabContent");

        this.listenTo(this.collection, 'add', this.render);
        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.collection, 'activation', this.renderContent);

        this.appTabViews = [];
        this.tabContentView = null;

        this.render();
      },

      events: {
        "click #tabs>li": "activateTab"
      },

      activateTab: function (e) {
        var tabView = $(e.currentTarget).data("view");
        this.collection.activateModel(tabView.model);
      },

      addOne: function (tab) {
        var view = new TabView({model: tab});
        this.appTabViews.push(view);
        this.$tabLabels.append(view.render().el);
      },

      renderContent: function () {
        var tab = this.collection.getActiveModel();

        if (this.tabContentView) {
          this.tabContentView.destroyView();
          this.tabContentView = null;
        }

        var view = new TabContentView({model: tab});
        this.tabContentView = view;
        this.$tabContent.html(view.render().el);
      },

      render: function () {
        _.each(this.appTabViews, function (childView) {
          childView.destroyView();
        });

        this.appTabViews = [];
        
        this.collection.each(this.addOne, this);

        return this;
      }
    });
  });