define("views/table/tableRow", ["views/view"], function (View) {
  return View.extend({
    model: null,

    tagName: "tr",


    initialize: function () {
      this.template = _.template($('#table-row-template').html());
      this.listenTo(this.model, "change", this.render);
    },

    render: function () {
      $(this.el).html(this.template(this.model.toJSON()));
      return this;
    }
  });
});