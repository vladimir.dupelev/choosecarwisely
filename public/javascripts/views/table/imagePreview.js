define("views/table/imagePreview", ["views/view"], function (View) {
  return View.extend({
    model: null,

    events: {
      "mouseover": "showPreview",
      "mouseleave": "hidePreview"
    },

    destroyView: function () {
      this.hidePreview();

      View.prototype.destroyView.call(this);
    },

    showPreview: function () {
      var $image = this.$el;
      var topPosition = $image.position().top;
      var leftPosition = $image.position().left;
      var imageWidth = $image.width();
      var gap = 10;
      var left = leftPosition + imageWidth + gap;

      this.previewDiv = $('<div class="popover"><img class="popover_image" src="' + this.model.get("img") + '"></div>');

      this.previewDiv.css("top", topPosition + "px");
      this.previewDiv.css("left", left + "px");

      this.previewDiv.insertAfter($image).hide().fadeIn();
    },

    hidePreview: function () {
      var self = this;
      if (self.previewDiv) {
        self.previewDiv.fadeOut("fast", function () {
          self.previewDiv.remove();
        });
      }
    },

    render: function () {
      return this;
    }
  });
});