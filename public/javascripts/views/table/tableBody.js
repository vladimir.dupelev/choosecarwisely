define("views/table/tableBody", ["views/view", "views/table/tableRow"], function (View, TableRow) {
  return View.extend({
    collection: null,
    model: null,

    initialize: function (options) {
      this.RowViewCtor = options.RowViewCtor;

      this.listenTo(this.collection, 'update', this.render);
      this.listenTo(this.collection, 'reset', this.render);

      this.rowViews = [];
    },

    destroyView: function () {
      _.each(this.rowViews, function (childView) {
        childView.destroyView();
      });

      this.rowViews = [];

      View.prototype.destroyView.call(this);
    },

    render: function () {
      var self = this;

      _.each(this.rowViews, function (childView) {
        childView.destroyView();
      });

      this.rowViews = [];

      this.$el.empty();

      this.collection.forEach(function (item) {
        if (self.model.filterItem && !self.model.filterItem(item)) {
          return;
        }

        var RowCtor = self.RowViewCtor || TableRow;
        var itemView =  new RowCtor({model: item});
        self.rowViews.push(itemView);
        self.$el.append(itemView.render().el);
      });

      return this;
    }
  });
});