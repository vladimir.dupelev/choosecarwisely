define("views/tabView", ["views/view"], function (View) {
  return View.extend({

    tagName: "li",

    template: _.template($('#item-template').html()),

    initialize: function () {
      this.listenTo(this.model, 'change:active', this.updateActive);
      this.listenTo(this.model, 'destroy', this.remove);

      $(this.el).data('view', this);
    },

    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
      this.updateActive();
      return this;
    },

    updateActive: function () {
      if (this.model.get("active")) {
        this.$el.addClass("active");
      } else {
        this.$el.removeClass("active");
      }

      return this;
    }
  });
});