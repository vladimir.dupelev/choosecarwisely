define("views/tabContentView", ["views/view"], function (View) {
  return View.extend({

    templates: {},

    initialize: function () {
      this.currentContentView = null;
    },

    destroyView: function () {
      if (this.currentContentView) {
        this.currentContentView.destroyView();
        this.currentContentView = null;
      }

      View.prototype.destroyView.call(this);
    },

    render: function () {
      var self = this;
      var contentName = this.model.get("content");

      if (self.templates[contentName]) {
        self.renderTemplate(contentName);
      } else {
        $.get('appTabs/' + contentName, function (data) {
          self.templates[contentName] = _.template(data);
          self.renderTemplate(contentName);
        }, 'html');
      }

      return this;
    },

    renderTemplate: function (contentName) {
      if (this.currentContentView) {
        this.currentContentView.destroyView();
        this.currentContentView = null;
      }

      var self = this;
      var template = this.templates[contentName];
      var innerElement = this.$el.html(template(this.model.toJSON()));

      require(["views/" + contentName + "/contentView"], function (ContentView) {
        self.currentContentView = new ContentView({ el: innerElement, model: self.model});
      });
    }
  });
});