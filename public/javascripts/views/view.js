define("views/view", function () {
  return Backbone.View.extend({

    destroyView: function () {
      if (this._destroyed) {
        throw new Error("Already destroyed");
      }

      this.undelegateEvents();
      this.$el.removeData().unbind();
      
      this.remove();
      Backbone.View.prototype.remove.call(this);

      this.$el.remove();
      this.$el = null;
      this.el = null;
      this._destroyed = true;
    }
  });
});