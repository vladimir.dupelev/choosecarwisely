define("views/carsTab/tableRow", ["views/view", "views/table/imagePreview"], function (View, ImagePreview) {
  return View.extend({
    model: null,

    tagName: "tr",

    initialize: function () {
      this.template = _.template(_.unescape($('#car-table-row-template').html()));
      this.listenTo(this.model, "change", this.render);
      this.imagePreview = null;
    },

    destroyView: function () {
      if (this.imagePreview) {
        this.imagePreview.destroyView();
        this.imagePreview = null;
      }

      View.prototype.destroyView.call(this);
    },

    events: {
      "click td>.action": "selectItem"
    },

    selectItem: function () {
      this.model.set("selected", true);
    },

    render: function () {
      $(this.el).html(this.template(this.model.toJSON()));

      this.imagePreview = new ImagePreview({
        model: this.model,
        el: $(this.el).find("img")
      });

      $(this.el).find(".action").prop('disabled', this.model.get('selected'));

      return this;
    }
  });
});