define("views/carsTab/contentView",
  ["views/view", "views/carsTab/filterView", "views/table/tableBody", "views/carsTab/tableRow"],
  function (View, FilterView, TableBody, TableRow) {
    return View.extend({
      model: null,

      initialize: function () {
        this.tableBody = null;
        this.filterView = null;

        this.listenTo(this.model, "change:filter", this.updateViews);

        this.render();
      },

      destroyView: function () {
        if (this.tableBody) {
          this.tableBody.destroyView();
          this.tableBody = null;
        }

        if (this.filterView) {
          this.filterView.destroyView();
          this.filterView = null;
        }

        View.prototype.destroyView.call(this);
      },

      render: function () {
        if (this.tableBody) {
          this.tableBody.destroyView();
          this.tableBody = null;
        }

        if (this.filterView) {
          this.filterView.destroyView();
          this.filterView = null;
        }

        this.filterView = new FilterView({
          el: $("#allCarsFilter"),
          model: this.model
        });

        this.tableBody = new TableBody({
          model: this.model,
          collection: this.model.get("carList"),
          el: this.$("#allCarsTable").children("tbody").first(),
          RowViewCtor: TableRow
        });

        this.updateViews();

        return this;
      },

      updateViews: function () {
       if (this.tableBody) {
         this.tableBody.render();
       }

        $("#allCarsCount").text(this.model.count());
        $("#allCarsTotalCount").text(this.model.totalCount());
      }
    });
  });