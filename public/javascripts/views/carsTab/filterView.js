define("views/carsTab/filterView", ["views/view"], function (View) {
  return View.extend({

    model: null,

    initialize: function () {
      this.listenTo(this.model, "change:filter", this.updateActive);
      this.updateActive();
    },

    events: {
      "click li": "activateFilter"
    },

    activateFilter: function (e) {
      var value = $(e.currentTarget).data("value");
      this.model.set("filter", value);

      e.stopImmediatePropagation();
    },

    render: function () {
      return this;
    },

    updateActive: function () {
      var brandFilter = this.model.get("filter");
      this.$el.children('li').each(function () {
        var $li = $(this);

        if ($li.data("value") === brandFilter) {
          $li.addClass("active");
        } else {
          $li.removeClass("active");
        }
      });

      return this;
    }
  });
});