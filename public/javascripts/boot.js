$(document).ready(function () {
  require.config({
    baseUrl: "javascripts"
  });

  require([
    "models/carsTab",
    "models/favTab",
    "models/chartTab",
    "models/appTabs",
    "views/appView",
    "router"], function (CarsTab, FavTab, ChartTab, AppTabs, AppView, Router) {

    function populateAppTabs() {

      return new AppTabs([
        new CarsTab({active: true, label: "All Cars"}),
        new FavTab({label: "Selected Cars"}),
        new ChartTab({label: "Selection Statistics"})
      ]);
    }

    var appTabs = populateAppTabs();

    new AppView({collection: appTabs});
    new Router({collection: appTabs});
    Backbone.history.start();
  });
});