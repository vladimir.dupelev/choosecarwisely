define("router", function () {
  var AppRouter = Backbone.Router.extend({
    routes: {},

    initialize: function (options) {
      var collection = options.collection;

      collection.each(function (model) {
        var route = model.get("route");

        this.route(route, function () {
          collection.activateModel(model);
        });
      }, this);

    }
  });

  return AppRouter;
});