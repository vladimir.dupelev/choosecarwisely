describe('Car Model', function () {

  it('should get instance of one car', function (done) {

    require(["models/domain/car"], function (Car) {
      var car = new Car();
      expect(car instanceof Backbone.Model).toBeTruthy();
      expect(car instanceof Backbone.Collection).toBeFalsy();
      done();
    });
  });

  it('should trigger event on car selection', function (done) {

    require(["models/domain/car"], function (Car) {
      var car = new Car();

      car.on("change:selected", function () {
        expect(car.get("selected")).toBeTruthy();
        done();
      });

      car.set("selected", true);

    });
  });


});



