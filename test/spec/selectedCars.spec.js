describe('Selected Cars List', function () {

  it('should save and fetch cars from LS', function (done) {

    require(["models/domain/selectedCarsList"], function (SelectedCarsList) {
      var cars = new SelectedCarsList(_.defaultCarList);

      cars.save();

      var newCars = new SelectedCarsList();
      newCars.fetch();

      var result = JSON.stringify(newCars.toJSON());
      var expected = JSON.stringify(_.defaultCarList);

      expect(result).not.toBe("[]");
      expect(result).toBe(expected);
      done();
    });
  });

  it('should delete removed models from LS', function (done) {
    require(["models/domain/selectedCarsList"], function (SelectedCarsList) {
      var cars = new SelectedCarsList(_.defaultCarList);

      cars.save();

      cars.remove(cars.at(0));

      cars.save();

      var newCars = new SelectedCarsList();
      newCars.fetch();

      var result = JSON.stringify(newCars.toJSON());
      var expected = JSON.stringify([_.defaultCarList[1]]);

      expect(result).not.toBe("[]");
      expect(result).toBe(expected);
      done();
    });
  });

  it('should auto destroy model', function (done) {
    require(["models/domain/selectedCarsList"], function (SelectedCarsList) {
      var cars = new SelectedCarsList(_.defaultCarList);

      cars.save();

      cars.at(1).destroy();

      var newCars = new SelectedCarsList();
      newCars.fetch();

      var result = JSON.stringify(newCars.toJSON());
      var expected = JSON.stringify([_.defaultCarList[0]]);

      expect(result).not.toBe("[]");
      expect(result).toBe(expected);
      done();
    });
  });

  it('should auto save on collection change', function (done) {
    require(["models/domain/selectedCarsList"], function (SelectedCarsList) {
      var cars = new SelectedCarsList(_.defaultCarList, {saveAllOnUpdate: true});
      cars.localStorage._clear();

      cars.remove(cars.at(0));

      var newCars = new SelectedCarsList();
      newCars.fetch();

      var result = JSON.stringify(newCars.toJSON());
      var expected = JSON.stringify([_.defaultCarList[1]]);

      expect(result).not.toBe("[]");
      expect(result).toBe(expected);
      done();
    });
  });

});



