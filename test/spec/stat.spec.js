describe('Stat Model', function () {

  it('should be Model', function (done) {

    require(["models/domain/stat"], function (Stat) {
      var stat = new Stat();
      expect(stat instanceof Backbone.Model).toBeTruthy();
      expect(stat instanceof Backbone.Collection).toBeFalsy();
      done();
    });
  });

  it('should fetch and update', function (done) {

    require(["models/domain/stat"], function (Stat) {
      var stat = new Stat();
      stat.localStorage._clear();
      stat.incrementUsage("brand1");
      stat.incrementUsage("brand2");
      stat.incrementUsage("brand2");

      var testStat = new Stat();

      expect(testStat.get("brand1")).toBe(1);
      expect(testStat.get("brand2")).toBe(2);
      done();
    });
  });

  it('should be integrated in selected cars', function (done) {

    require(["models/domain/stat", "models/domain/selectedCarsList"], function (Stat, SelectedCarsList) {
      var stat = new Stat();
      stat.localStorage._clear();
      var justCars = new SelectedCarsList(_.unselectedCarList);
      justCars.localStorage._clear();

      justCars.listenTo(justCars, "change:selected", function (car) {
        if (car.get("selected")) {
          stat.incrementUsage(car.get("brand"));
        }
      }, this);

      justCars.at(0).set("selected", true);
      justCars.at(1).set("selected", true);
      justCars.at(1).set("selected", false);
      justCars.at(1).set("selected", true);

      var testStat = new Stat();

      expect(testStat.get(justCars.at(0).get("brand"))).toBe(1);
      expect(testStat.get(justCars.at(1).get("brand"))).toBe(2);
      done();
    });
  });

});



