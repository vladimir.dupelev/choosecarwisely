describe('Collections merge', function () {

  it('should add two different models', function (done) {

    require(["models/domain/selectedCarsList"], function (SelectedCarsList) {
      var dest = new SelectedCarsList([_.defaultCarList[0]]);
      var source = new SelectedCarsList([_.defaultCarList[1]]);

      dest.add(source.toJSON());

      var result = JSON.stringify(dest.toJSON());
      var expected = JSON.stringify(_.defaultCarList);

      expect(result).not.toBe("[]");
      expect(result).toBe(expected);

      done();
    });
  });

  it('should not alternate existed models', function (done) {

    require(["models/domain/selectedCarsList"], function (SelectedCarsList) {
      var dest = new SelectedCarsList(_.defaultCarList);

      var source = new SelectedCarsList([_.defaultCarList[0]]);
      var sourceCar = source.at(0);
      sourceCar.set("someProp", "someValue");
      sourceCar.set("model", "testModel");

      dest.add(source.toJSON());

      var result = JSON.stringify(dest.toJSON());
      var expected = JSON.stringify(_.defaultCarList);

      expect(result).not.toBe("[]");
      expect(result).toBe(expected);

      done();
    });
  });


  it('should alternate existed models', function (done) {

    require(["models/domain/selectedCarsList"], function (SelectedCarsList) {
      var dest = new SelectedCarsList(_.defaultCarList);
      var destCar = dest.at(0);
      destCar.set("model", "testModel");

      var source = new SelectedCarsList([_.defaultCarList[0]]);

      dest.add(source.toJSON(), { merge: true});

      var result = JSON.stringify(dest.toJSON());
      var expected = JSON.stringify(_.defaultCarList);

      expect(result).not.toBe("[]");
      expect(result).toBe(expected);

      done();
    });
  });
  
});



