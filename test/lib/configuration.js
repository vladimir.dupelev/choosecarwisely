require.config({
  baseUrl: "javascripts"
});

_.defaultCarList =
  [
    {
      id: 5,
      brand: "Volvo",
      model: "V60 Cross Country",
      selected: true
    },
    {
      id: 11,
      brand: "Ford",
      model: "Ford Fiesta",
      selected: true
    }
  ];

_.unselectedCarList =
  [
    {
      id: 5,
      brand: "Volvo",
      model: "V60 Cross Country"
    },
    {
      id: 11,
      brand: "Ford",
      model: "Ford Fiesta"
    }
  ];

_.realCarList =
  [
    {
      "id": 1,
      "brand": "Volvo",
      "model": "XC90",
      "desc": "СОЗДАН ДЛЯ БУДУЩЕГО\nАбсолютно новый XC90",
      "img": "http://assets.volvocars.com/ru/~/media/russia/cars/model-lineup/xc90.jpg?w=570"
    }
  ];