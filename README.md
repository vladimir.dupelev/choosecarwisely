## Build and Run

install Nodejs > 4.4

checkout master branch 

cd project folder 

run `npm install`

run `npm test`

## Serve project

run `npm start`

open browser with [localhost:3000]()
