var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var stylus = require('stylus');
var nib = require('nib');
var i18n = require('i18n');

var routes = require('./routes/index');
var appTabs = require('./routes/appTabs');
var dataRoutes = require('./routes/data');

var allCarsService = require('./services/allCars')({
  data: "./data/cars.json"
});

dataRoutes.dataService = allCarsService;

// i18n configure
i18n.configure({
  locales: ['en'],
  defaultLocale: 'en',
  cookie: 'choosecarwisely',
  directory: __dirname + '/locales',
  updateFiles: false
});

var app = express();
app.disable('x-powered-by');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.disable('etag');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(i18n.init);

function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .use(nib());
}

app.use(stylus.middleware(
  {
    src: path.join(__dirname, 'public'),
    compile: compile
  }
));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/appTabs', appTabs);
app.use('/data', dataRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  res.render('404', {title: res.__("404")});
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
