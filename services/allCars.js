var fs = require("fs");
var _ = require("underscore");

function readJsonFileSync(filepath) {
  var file = fs.readFileSync(filepath, "utf8");
  return JSON.parse(file);
}

function AllCarsService(options) {
  if (!(this instanceof AllCarsService)) {
    return new AllCarsService(options);
  }

  options = options || {};

  var jsonDataFile = options.data;
  this.data = readJsonFileSync(jsonDataFile);
}

AllCarsService.prototype.getBrandList = function () {
  var map = _.map(this.data, function (element) {
    return element.brand;
  });

  return _.uniq(map);
};

AllCarsService.prototype.getCarList = function (carId) {
  if (carId) {
    return this.data.find(function (element) {
      return element.id == carId;
    });
  } else {
    return this.data;
  }
};

module.exports = AllCarsService;
