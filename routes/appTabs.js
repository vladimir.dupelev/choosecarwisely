var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/carsTab', function(req, res, next) {
  res.render('appTabs/carsTab');
});

router.get('/favTab', function(req, res, next) {
  res.render('appTabs/favTab');
});

router.get('/chartTab', function(req, res, next) {
  res.render('appTabs/chartTab');
});

module.exports = router;
