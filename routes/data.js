var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/brandList', function (req, res, next) {
  res.send(router.dataService.getBrandList());
});

router.get('/carList/:carId?', function (req, res, next) {
  res.send(router.dataService.getCarList(req.params.carId));
});

module.exports = router;
